package com.bignerdranch.android.fcisquare;

/**
 * Created by Yara mansour on 3/25/2016.
 */
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by Yara mansour on 3/25/2016.
 */

public class httpRequest {

    public JSONObject loginrequest(String mail , String pass){
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppostreq = new HttpPost("http://project-fcisquareapp.rhcloud.com/FCISquare/rest/login");
        JSONObject obj = new JSONObject();
        try {
            obj.put("email",mail);
            obj.put("pass",pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = obj.toString();
        try {
            HttpEntity entity = new StringEntity(request);
            httppostreq.setEntity(entity);
            HttpResponse response = httpclient.execute(httppostreq);
            //InputStream is = response.getEntity().getContent();
            String json = EntityUtils.toString(response.getEntity());
            JSONObject result = new JSONObject(json);
            return result ;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
    public JSONObject signuprequest(String name ,String mail , String pass){
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppostreq = new HttpPost("http://project-fcisquareapp.rhcloud.com/FCISquare/rest/signup");
        JSONObject obj = new JSONObject();
        try {
            obj.put("name",name);
            obj.put("email",mail);
            obj.put("pass",pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = obj.toString();
        try {
            HttpEntity entity = new StringEntity(request);
            httppostreq.setEntity(entity);
            HttpResponse response = httpclient.execute(httppostreq);
            //InputStream is = response.getEntity().getContent();
            String json = EntityUtils.toString(response.getEntity());
            JSONObject result = new JSONObject(json);
            return result ;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
    public JSONObject followrequest(String mail , int id){
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppostreq = new HttpPost("http://project-fcisquareapp.rhcloud.com/FCISquare/rest/follow");
        JSONObject obj = new JSONObject();
        try {
            obj.put("reciever_mail",mail);
            obj.put("sender_id",id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = obj.toString();
        try {
            HttpEntity entity = new StringEntity(request);
            httppostreq.setEntity(entity);
            HttpResponse response = httpclient.execute(httppostreq);
            //InputStream is = response.getEntity().getContent();
            String json = EntityUtils.toString(response.getEntity());
            JSONObject result = new JSONObject(json);
            return result ;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
    public JSONObject unfollowrequest(String mail , int id){
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppostreq = new HttpPost("http://project-fcisquareapp.rhcloud.com/FCISquare/rest/unfollow");
        JSONObject obj = new JSONObject();
        try {
            obj.put("reciever_mail",mail);
            obj.put("sender_id",id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = obj.toString();
        try {
            HttpEntity entity = new StringEntity(request);
            httppostreq.setEntity(entity);
            HttpResponse response = httpclient.execute(httppostreq);
            //InputStream is = response.getEntity().getContent();
            String json = EntityUtils.toString(response.getEntity());
            JSONObject result = new JSONObject(json);
            return result ;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
    public JSONObject getfollowingrequest( int id){
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppostreq = new HttpPost("http://project-fcisquareapp.rhcloud.com/FCISquare/rest/getfollowing");
        JSONObject obj = new JSONObject();
        try {
            obj.put("follower_id",id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = obj.toString();
        try {
            HttpEntity entity = new StringEntity(request);
            httppostreq.setEntity(entity);
            HttpResponse response = httpclient.execute(httppostreq);
            //InputStream is = response.getEntity().getContent();
            String json = EntityUtils.toString(response.getEntity());
            JSONObject result = new JSONObject(json);
            return result ;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
