package com.bignerdranch.android.fcisquare;

/**
 * Created by Yara mansour on 3/24/2016.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class signin extends Activity {
    Bundle extra;
    Button enter ;
    EditText email , password;
    String mail , pass , name;
    int id ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);
        extra=getIntent().getExtras();
        enter=(Button)findViewById(R.id.signIN);
        email=(EditText)findViewById(R.id.mail);
        password=(EditText)findViewById(R.id.pass);
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mail = email.getText().toString();
                pass = password.getText().toString();
                httpRequest login = null;
                JSONObject obj= login.loginrequest(mail, pass);
                new asynctask().execute();
                try {
                    id=obj.getInt("id");
                    name = obj.getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(id != 0){
                    Intent intent=new Intent(signin.this,profile.class);
                    intent.putExtra("id",id);
                    startActivity(intent);
                }

            }
        });
    }
}

