package com.bignerdranch.android.fcisquare;

/**
 * Created by Yara mansour on 3/24/2016.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class follow extends Activity {
    Button follow ,unfollow;
    EditText email;
    String mail;
    int status , id;
    Bundle extra;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        extra = getIntent().getExtras();
        id =extra.getInt("id");
        follow = (Button) findViewById(R.id.follow);
        unfollow = (Button) findViewById(R.id.unfollow);
        email = (EditText) findViewById(R.id.followmail);
        follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mail=email.getText().toString();
                httpRequest follow = null;
                JSONObject obj= follow.followrequest(mail, id);
                try {
                    status=obj.getInt("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
        unfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mail=email.getText().toString();
                httpRequest unfollow = null;
                JSONObject obj= unfollow.unfollowrequest(mail, id);
                try {
                    status=obj.getInt("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }
}
