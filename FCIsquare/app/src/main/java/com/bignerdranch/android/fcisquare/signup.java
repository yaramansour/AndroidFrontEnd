package com.bignerdranch.android.fcisquare;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.app.Activity;

import com.bignerdranch.android.fcisquare.R;
import com.bignerdranch.android.fcisquare.profile;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Yara mansour on 3/24/2016.
 */
public class signup extends Activity{
    Bundle extra;
    Button signup ;
    EditText email , password,user_name;
    String mail , pass, name ;
    int id ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        extra=getIntent().getExtras();
        signup=(Button)findViewById(R.id.signup);
        email=(EditText)findViewById(R.id.mail);
        password=(EditText)findViewById(R.id.pass);
        user_name=(EditText)findViewById(R.id.name);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mail =email.getText().toString();
                pass= password.getText().toString();
                name=user_name.getText().toString();
                httpRequest signup = null;
                JSONObject obj= signup.signuprequest(name, mail, pass);

                try {
                    id=obj.getInt("id");
                    name = obj.getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent=new Intent(signup.this,profile.class);
                intent.putExtra("id",id);
                startActivity(intent);
            }

        });
    }
}


