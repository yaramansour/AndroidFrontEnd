package com.bignerdranch.android.fcisquare;

import android.os.AsyncTask;

import org.json.JSONObject;

/**
 * Created by Yara mansour on 3/27/2016.
 */
public class asynctask  extends AsyncTask<String,Void,JSONObject>{
    @Override
    protected JSONObject doInBackground(String... params) {
        httpRequest login = new httpRequest();
        JSONObject obj =login.loginrequest(params[0],params[1]);
        return obj;
    }
}
