package com.bignerdranch.android.fcisquare;

/**
 * Created by Yara mansour on 3/24/2016.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class profile extends Activity {
    Bundle extra;
    Button follow , list ;
    int id ;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        extra=getIntent().getExtras();
        id =extra.getInt("id");
        follow=(Button)findViewById(R.id.follow);
        list=(Button)findViewById(R.id.list);

        follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(profile.this,follow.class);
                intent.putExtra("id",id);
                startActivity(intent);
            }

        });
        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(profile.this,list.class);
                intent.putExtra("id",id);
                startActivity(intent);
            }

        });
    }
}

